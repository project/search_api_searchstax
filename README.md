Search API SearchStax
=====================

SearchStax connector plugin for Search API Solr.
The documentation is available at
https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/searchstax-search-api
  
